package net.mmgg.sso.server.config;

import lombok.Data;

import java.io.Serializable;

/**
 * @author phil
 * @version 2.0
 * @date 2023-09-04 20:47
 */
@Data
public class GndsToken implements Serializable {
    /**
     * 原GNDS的Token
     */
    private String token;
    /**
     * cepToken
     */
    private CepToken cepToken;
    /**
     * Cep的用户信息
     */
    private CepUserInfo cepUserInfo;
    //可以增加其他服务器端返回的信息
}