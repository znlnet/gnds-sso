package net.mmgg.sso.server.config;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author phil
 * @version 2.0
 * @date 2023-09-04 18:37
 */
@Data
public class CepUserInfo implements Serializable {
    private String openid;
    private String username;
    private String realName;
    private String mobile;
    private String email;
    private int gender;
    private Date birthday;
    private String avatarUrl;
}