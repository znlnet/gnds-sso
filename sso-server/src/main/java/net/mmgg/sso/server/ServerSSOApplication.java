package net.mmgg.sso.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author phil
 * @version 2.0
 * @date 2023-09-04 17:08
 */
@SpringBootApplication
public class ServerSSOApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServerSSOApplication.class,args);
    }
}
