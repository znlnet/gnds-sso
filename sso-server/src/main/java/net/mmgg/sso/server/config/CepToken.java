package net.mmgg.sso.server.config;

import lombok.Data;

import java.io.Serializable;

/**
 * @author phil
 * @version 2.0
 * @date 2023-09-04 18:34
 */
@Data
public class CepToken implements Serializable {
    private String access_token;
    private String refresh_token;
    private Integer expires_in;
    private String openid;
}