package net.mmgg.sso.server.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author phil
 * @version 2.0
 * @date 2023-09-04 17:55
 */
@Data
@Component
@ConfigurationProperties(prefix = "sso.cep")
public class SsoConfig {
    private String clientId;
    private String clientSecret;
    private String url;
    private String redirectUri;
}
