package net.mmgg.sso.server;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import net.mmgg.sso.server.config.CepToken;
import net.mmgg.sso.server.config.CepUserInfo;
import net.mmgg.sso.server.config.GndsToken;
import net.mmgg.sso.server.config.SsoConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @author phil
 * @version 2.0
 * @date 2023-09-04 17:53
 */
@Slf4j
@RestController
@RequestMapping
public class SsoLoginController {

    private Map<String, String> clientMap = new HashMap<>();

    @Autowired
    private SsoConfig config;

    /**
     * sso跳转
     *
     * @param response
     * @param sec
     * @param client
     * @throws IOException
     */
    @GetMapping("sso/login")
    public void login(HttpServletResponse response, @RequestParam String sec, @RequestParam String client) throws IOException {
        clientMap.put(client, sec);
        ///login?client_id=CLIENT_ID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE
        String param = URLUtil.buildQuery(Map.of(
                "client_id", config.getClientId(),
                "redirect_uri", config.getRedirectUri(),
                "response_type", "code",
                "state", client), Charset.defaultCharset());
        String url = config.getUrl() + "/ent/login?" + param;
        response.sendRedirect(url);
    }

    /**
     * sso的回调
     *
     * @param response
     * @param code
     * @param state
     * @throws IOException
     */
    @GetMapping("callback")
    public void callback(HttpServletResponse response, String code, String state) throws IOException {
        log.info("回调参数===>code:{},state:{}",code,state);
        if (StrUtil.isNotBlank(code) && StrUtil.isNotBlank(state) && clientMap.containsKey(state)) {
            Map<String, Object> param = Map.of(
                    "client_id", config.getClientId(),
                    "client_secret", config.getClientSecret(),
                    "redirect_uri", config.getRedirectUri(),
                    "response_type", "authorization_code",
                    "code", code);
            HttpRequest request = HttpUtil.createPost(config.getUrl() + "/oauth2/token");
            HttpResponse httpResponse = request.body(JSONUtil.toJsonStr(param)).execute();
            if (httpResponse.isOk()) {
                String json = httpResponse.body();
                log.info("请求获取到Token：{}" , json);
                if (StrUtil.isNotBlank(json)) {
                    CepToken token = JSONUtil.toBean(json, CepToken.class);
                    if (null != token && StrUtil.isNotBlank(token.getAccess_token())) {
                        CepUserInfo userInfo = getUserInfo(token.getAccess_token());
                        log.info("获取到用户信息：{}" ,JSONUtil.toJsonStr(userInfo));
                        //TODO 这里可以去判断用户是否有效...略

                        RSA rsa = SecureUtil.rsa(null, clientMap.get(state));

                        GndsToken gndsToken = new GndsToken();
                        gndsToken.setCepToken(token);
                        gndsToken.setCepUserInfo(userInfo);
                        //TODO 按原有逻辑去生成Token
                        gndsToken.setToken("这个是GNDS服务器端生产的token，这里只是演示，实际按原来的逻辑生成即可");
                        //生成随机字符串16位AES密钥
                        String aesKey = RandomUtil.randomString(16);
                        String ciphertext = SecureUtil.aes(aesKey.getBytes(StandardCharsets.UTF_8)).encryptBase64(JSONUtil.toJsonStr(gndsToken));
                        //用RSA加密Aes的的密钥
                        String aesKey_Sec = rsa.encryptBase64(aesKey, StandardCharsets.UTF_8, KeyType.PublicKey);
                        //传递AES密钥的密文和通过AES加密后的数据密文给客户端
                        response.sendRedirect("http://127.0.0.1:8081/client/callback?ciphertext=" + ciphertext + "&key=" + aesKey_Sec);
                    }
                }
            }
        }else {
            log.info("回调参数不完整！");
        }
    }

    private CepUserInfo getUserInfo(String token) {
        String json = HttpUtil.post(config.getUrl() + "/api/user/info2?access_token", token);
        if (StrUtil.isNotBlank(json)) {
            CepUserInfo userInfo = JSONUtil.toBean(json, CepUserInfo.class);
            if (null != userInfo) {
                //TODO 这里还可以去数据库中验证用户是否真的存在
                return userInfo;
            }
        }
        return null;
    }
}