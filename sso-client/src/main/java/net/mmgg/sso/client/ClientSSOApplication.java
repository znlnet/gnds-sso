package net.mmgg.sso.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author phil
 * @version 2.0
 * @date 2023-09-04 17:08
 */
@SpringBootApplication
public class ClientSSOApplication {
    public static void main(String[] args) {
        SpringApplication.run(ClientSSOApplication.class,args);
    }
}
