package net.mmgg.sso.client;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @author phil
 * @version 2.0
 * @date 2023-09-04 17:37
 */
@Slf4j
@RequestMapping
@RestController
public class LoginController {

    private RSA rsa = new RSA();
    @Value("${gnds.server.url}")
    private String gndsServerUrl;

    /**
     * 传递RSA公钥给服务器端   18512405433
     * 客户端ID，此处是模拟的10位数字客户端ID
     */
    @GetMapping("client/login")
    public void toLogin(HttpServletResponse response) throws Exception {
        String param = URLUtil.buildQuery(Map.of("sec", rsa.getPublicKeyBase64(), "client", RandomUtil.randomNumbers(10)), StandardCharsets.UTF_8);
        response.sendRedirect(gndsServerUrl + "/sso/login?" + param);

    }

    /**
     * 服务器端通过公钥加密后的密码进行解密aes的密钥，然后通过aes的密钥解密数据密文
     *
     * @param ciphertext 数据密文（通过aes的密钥加密后的密文）
     * @param key        aes的密钥（密文形式）
     */
    @GetMapping("client/callback")
    public void callbak(String ciphertext, String key) {
        log.info("回调客户端的参数===>ciphertext:{},key:{}", ciphertext, key);
        if (StrUtil.isNotBlank(ciphertext) && StrUtil.isNotBlank(key)) {
            String rawAesKey = rsa.decryptStr(key, KeyType.PrivateKey, StandardCharsets.UTF_8);
            String rawJson = SecureUtil.aes(rawAesKey.getBytes(StandardCharsets.UTF_8)).decryptStr(ciphertext);
            log.info("客户端解密后的参数：{}" , rawJson);
            //客户端存储openid和accessToken

        }
    }
}